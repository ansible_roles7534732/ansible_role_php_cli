php-cli ansible role
=========

Installs php-cli

Requirements
------------

—

Role Variables
--------------

See defaults/main/*

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install php-cli
  hosts: app_vms
  become: true
  gather_facts: true
  roles:
    - php_cli
      tags:
        - role_php_cli
```
License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
